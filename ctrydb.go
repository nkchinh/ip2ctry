package ip2ctry

import(
	"os"
	"bufio"
	"strings"
)

func loadCtry(fn string)(map[string]string, error){

	file, err := os.Open(fn)
  if err != nil {
		return nil, err
  }
  defer file.Close()

	res:=make(map[string]string)

  scanner := bufio.NewScanner(file)
  for scanner.Scan() {
		arr:=strings.Split(scanner.Text(),",")
		res[strings.ToUpper(arr[1])]=arr[0]
  }

  if err := scanner.Err(); err != nil {
    return nil, err
  }

	return res, nil
}
