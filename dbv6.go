package ip2ctry

import(
	"os"
	"net"
	"strings"
	"encoding/csv"
)

// load ipv6 data
func loadV6(fn string)([]Range, error){
	// open v4 data file
	f,err:=os.Open(fn)
	// check error
	if err!=nil{
		return nil, err
	}
	// get all rata rows
	lines, err := csv.NewReader(f).ReadAll()
	// check error
	if err!=nil{
		return nil, err
	}
	// init data store
	llen:=len(lines)
	data:=make([]Range,llen)
	cr:=0
	// get data
	for _, line := range lines {
		//skip empty line
		if len(line[0])==0{
			continue
		}

		// skip comment line
		if line[0][:1]=="#"{
			continue
		}

		ft:=strings.Split(line[0],"-")

		if len(ft)<2{
			continue
		}

		// parse ip from
		data[cr].From=net.ParseIP(ft[0])
		// check error
		if data[cr].From==nil{
			continue
		}
		// parse ip to
		data[cr].To=net.ParseIP(ft[1])
		// check error
		if data[cr].To==nil{
			continue
		}

		// get country code
		data[cr].Ctry=line[1]

		cr++
	}
	// remove empty rows
	if llen>cr{
		data=data[:cr]
	}

	return data, nil
}
