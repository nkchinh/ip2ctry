package ip2ctry

import(
	"os"
	"strconv"
	"encoding/csv"
)

// load ipv4 data
func loadV4(fn string)([]Range, error){
	// open v4 data file
	f,err:=os.Open(fn)
	// check error
	if err!=nil{
		return nil, err
	}
	// get all rata rows
	lines, err := csv.NewReader(f).ReadAll()
	// check error
	if err!=nil{
		return nil, err
	}
	// init data store
	llen:=len(lines)
	data:=make([]Range,llen)
	cr:=0
	// get data
	for _, line := range lines {
		//skip empty line
		if len(line[0])==0{
			continue
		}

		// skip comment line
		if line[0][:1]=="#"{
			continue
		}

		// parse ip from
		data[cr].From,err=decStrToBytes(line[0])
		// check error
		if err!=nil{
			continue
		}
		// parse ip to
		data[cr].To,err=decStrToBytes(line[1])
		// check error
		if err!=nil{
			continue
		}

		// get country code
		data[cr].Ctry=line[4]

		cr++
	}
	// remove empty rows
	if llen>cr{
		data=data[:cr]
	}

	return data, nil
}

func decStrToBytes(str string)([]byte, error){
	ui64,err:=strconv.ParseUint(str, 10, 32)

	if err!=nil{
		return nil, err
	}

	arr:=make([]byte,4)
	arr[0]=byte(uint32(ui64>>24)&0xFF)
	arr[1]=byte(uint32(ui64>>16)&0xFF)
	arr[2]=byte(uint32(ui64>>8)&0xFF)
	arr[3]=byte(ui64&0xFF)

	return arr, nil
}
