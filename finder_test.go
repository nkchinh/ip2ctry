package ip2ctry

import(
	"os"
	"fmt"
	"testing"
)

func TestAll(t *testing.T){

	hp:=os.Getenv("HOME")
	finder:=NewFinder(
		hp+"/Downloads/IpToCountry.csv",
		hp+"/Downloads/IpToCountry.6R.csv",
		hp+"/Downloads/country-codes.txt")

	err:=finder.Load()
	if err!=nil{
		fmt.Printf("error load data files: %s \n", err.Error())
		t.Error(err)
		return
	}

	testV4(finder, t)
	testV6(finder, t)
	testCtryName(finder, t)
}

func testV4(finder *Finder, t *testing.T){
	fmt.Println("find ip 5.226.31.255")
	ctry:=finder.Find("159.225.131.218")
	fmt.Println(ctry)
}

func testV6(finder *Finder, t *testing.T){
	fmt.Println("find ip 2001:420:1101:1::a")
	ctry:=finder.Find("2001:cdba:0000:0000:0000:0000:3257:9652")
	fmt.Println(ctry)
}

func testCtryName(finder *Finder, t *testing.T){
	cn,err:=finder.CountryName("TR")
	if err !=nil{
		fmt.Println("error get country name")
		t.Error(err)
		return
	}
	fmt.Printf("TR: %s\n", cn)
	if cn!="TURKEY"{
		fmt.Println("invalid result")
		t.Error("invalid result")
	}


	cn,err=finder.CountryName("VN")
	if err !=nil{
		fmt.Println("error get country name")
		t.Error(err)
		return
	}
	fmt.Printf("VN: %s\n", cn)
	if cn!="VIET NAM"{
		fmt.Println("invalid result")
		t.Error("invalid result")
	}


	cn,err=finder.CountryName("XX")
	if err !=nil{
		fmt.Println("error get country name")
		t.Error(err)
		return
	}
	fmt.Printf("XX: %s\n", cn)
	if cn!=""{
		fmt.Println("invalid result")
		t.Error("invalid result")
	}
}
