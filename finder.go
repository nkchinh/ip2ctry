package ip2ctry

import(
	"bytes"
	"net"
	"strings"
	"errors"
)

const CODE_UNKNOWN string = "ZZ"

// location finder
type Finder struct{
	v4fn string
	v6fn string
	ctryfn string
	v4data []Range
	v6data []Range
	ctrydata map[string]string
}

type Range struct{
	From []byte
	To []byte
	Ctry string
}

// create new location finder
func NewFinder(v4dbfile string, v6dbfile string, ctryfile string)*Finder{
	return &Finder{v4dbfile, v6dbfile, ctryfile, nil, nil, nil}
}

// load data
func (this *Finder)Load() error{
	var err error

	// if has v4 data
	if(this.v4fn!=""){
		this.v4data,err=loadV4(this.v4fn)
		if err!=nil{
			return err
		}
	}
	// if has v6 data
	if(this.v6fn!=""){
		this.v6data, err=loadV6(this.v6fn)
		if err!=nil{
			return err
		}
	}
	// load country data
	if(this.ctryfn!=""){
		this.ctrydata,err=loadCtry(this.ctryfn)
		if err!=nil{
			return err
		}
	}

	return nil
}

func (this *Finder)Find(ipaddr string)string{
	addr:=net.ParseIP(ipaddr)

	if addr==nil{
		return CODE_UNKNOWN
	}

	v4:=addr.To4()
	if v4==nil{
		// is v6
		return this.find(addr,6)
	}

	return this.find(v4,4)
}

func (this *Finder)find(ipaddr []byte, v int)string{
	var data []Range
	if v==4{
		data=this.v4data
	}else{
		data=this.v6data
	}

	if data==nil{
		return CODE_UNKNOWN
	}

	dl:=len(data)

	/*or _,r:=range data{
		if bytes.Compare(r.From, ipaddr)>0{
			return CODE_UNKNOWN
		}else if bytes.Compare(r.To, ipaddr)>=0{
			return r.Ctry
		}
	}*/

	// binary serach
	if dl>0{

		left:=0
		right:=dl-1

		for left<=right{
			mid:=(left+right)/2

			frc:=bytes.Compare(data[mid].From, ipaddr)

			if frc<=0 &&
				bytes.Compare(data[mid].To, ipaddr)>=0{

				return data[mid].Ctry
			}else if frc>0 {
				right=mid-1
			}else if frc<0{
				left=mid+1
			}
		}

	}

	// return unknow
	return CODE_UNKNOWN
}

func (this *Finder) CountryName(ctryCode string)(string, error){
	if this.ctrydata==nil{
		return "", errors.New("Coutry data was not loaded")
	}

	cn,ok:=this.ctrydata[strings.ToUpper(ctryCode)]
	if ok{
		return cn, nil
	}else{
		return "", nil
	}
}
